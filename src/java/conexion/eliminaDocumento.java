/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexion;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mongodb.client.MongoDatabase; 
import com.mongodb.MongoClient; 
import com.mongodb.MongoCredential;  
import org.bson.*;
import com.mongodb.client.FindIterable; 
import com.mongodb.client.MongoCollection; 
import com.mongodb.client.MongoDatabase;  
import com.mongodb.client.model.Filters;
import javax.servlet.RequestDispatcher;
import org.bson.types.ObjectId;

/**
 *
 * @author XMY9460
 */
@WebServlet(name = "eliminaDocumento", urlPatterns = {"/eliminaDocumento"})
public class eliminaDocumento extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       String id=request.getParameter("id");
        
           // Se crea el cliente Mongo 
      MongoClient mongo = new MongoClient( "localhost" , 27017 ); 
     // Se crea la credencial admin=usuario, test=db, pass=password
      MongoCredential credential = MongoCredential.createCredential("admin", "test", "pass".toCharArray()); 
       
      System.out.println("Se conecto"+ credential.getUserName());
      
        // Accesar a la base
      MongoDatabase database = mongo.getDatabase("test"); 
       // obtiene la coleccion
      MongoCollection<Document> collection = database.getCollection("midocs");
      
      if(id!=null && id!=""){
          //elimina documento por ID
      collection.deleteOne(new Document("_id", new ObjectId(id)));
      }
      
        //try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
           // out.println("Conexion a MongoDB </br>");
           // out.println("Se conecto: "+ credential.getUserName()+"</br>");
         //   out.println("DB: "+database.getName()+"</br>");
       //     out.println("Se elimina el documento con ID: "+id+"</br></br>");            
     //}
      request.setAttribute("ID", id);
      RequestDispatcher view = request.getRequestDispatcher("elimina.jsp");
      view.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
