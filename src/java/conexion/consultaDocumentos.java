package conexion;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.mongodb.client.MongoDatabase; 
import com.mongodb.MongoClient; 
import com.mongodb.MongoCredential;  
import org.bson.*;
import com.mongodb.client.FindIterable; 
import com.mongodb.client.MongoCollection; 
import com.mongodb.client.MongoDatabase;  
import java.util.ArrayList;

import java.util.Iterator;
import javax.servlet.RequestDispatcher;
import org.json.JSONObject;

/**
 *
 * @author XMY9460
 */
@WebServlet(name = "consultaDocumentos", urlPatterns = {"/consultaDocumentos"})
public class consultaDocumentos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String parametro=request.getParameter("json");
        
        
        
        
           // Se crea el cliente Mongo 
      MongoClient mongo = new MongoClient( "localhost" , 27017 ); 
     // Se crea la credencial admin=usuario, test=db, pass=password
      MongoCredential credential = MongoCredential.createCredential("admin", "test", "pass".toCharArray()); 
       
      System.out.println("Se conecto"+ credential.getUserName());
      
       // Accesar a la base
      MongoDatabase database = mongo.getDatabase("test"); 
      
       //Crear una coleccion 
      //database.createCollection("micoleccion"); 
      
      
       // obtiene la coleccion
      MongoCollection<Document> collection = database.getCollection("midocs");
      
      ///if(parametro!=null && parametro!=""){
      ///JSONObject jo = new JSONObject(parametro);
      //crea nuevo documento
      //Document document = new Document("Documento", "MongoDB") 
      //.append("user", jo.getString("user"))
      //.append("descripcion", jo.getString("descripcion")); 
       //lo inserta en la coleccion
      //collection.insertOne(document); 
      //System.out.println("El documento se inserto"); 
      
      //}
      // crea un objeto iterable con los documentos de la base 
      FindIterable<Document> iterDoc = collection.find(); 
      int i = 1; 

      // obtiene el iterador
      Iterator it = iterDoc.iterator(); 
    
      ArrayList<String> documentos = new ArrayList<>();
      
      //System.out.println("Credentials ::"+ credential);
        //try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
          //  out.println("Conexion a MongoDB </br>");
           // out.println("Se conecto: "+ credential.getUserName()+"</br>");
           // out.println("DB: "+database.getName()+"</br>");
           // out.println("JSON: "+parametro+"</br></br>");
            
           // out.println("Documentos en la base de datos </br>");
            
            //muestra cada uno de los documentos en la coleccion
           while (it.hasNext()) {  
         documentos.add(it.next().toString());
      i++; 
      }
      request.setAttribute("user", credential.getUserName());
      request.setAttribute("db", database.getName());
      request.setAttribute("doc", parametro);
      request.setAttribute("documentos", documentos);
      
      RequestDispatcher view = request.getRequestDispatcher("consulta.jsp");
      view.forward(request, response);  
     //}
     
       
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
