<%-- 
    Document   : index
    Created on : Sep 26, 2018, 11:03:02 AM
    Author     : XMY9460
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Conexion Mongo DB</title>
    </head>
    <body>
         <nav class="navbar navbar-dark bg-primary">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#"><font color="white">Servlet Mongo DB</font></a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#"><font color="white">Inicio</font></a></li>
    </ul>
  </div>
</nav>
        <div class="container-fluid row">
            <div class="col-sm-6">
        <label>Insertar Documento</label>
        <form action="insertaDocumento" method="POST">
            <input class="form-control" type="text" name="json"/>
            <input class="btn" type="submit" value="Insertar documento"/>
        </form>
        <br>
        
        <label>Eliminar Documento</label>
        <form action="eliminaDocumento">
            <input class="form-control" type="text" name="id"/>
            <input class="btn" type="submit" value="Eliminar por ID"/>
        </form>
        <br>
        <label>Consultar Documentos</label>
        <form action="consultaDocumentos">
            <input class="btn" type="submit" value="Consultar elementos de la BD"/>
        </form>
        </div>
        </div>
    </body>
</html>
