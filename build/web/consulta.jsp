<%-- 
    Document   : consulta
    Created on : Oct 3, 2018, 4:03:22 PM
    Author     : XMY9460
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Documentos</title>
    </head>
    <body>
         <nav class="navbar navbar-dark bg-primary">
  <div class="container-fluid">
    <div class="navbar-header">
        <a class="navbar-brand" href="http://localhost:8080/mongoDB/"><font color="white">Servlet Mongo DB</font></a>
    </div>
    <ul class="nav navbar-nav">
        <li class="active"><a href="#"><font color="white">Consulta documentos</font></a></li>
    </ul>
  </div>
</nav>
        <div class="container-fluid">
        <h2>Usuario: ${user}</h2>
        <h2>Base de datos: ${db}</h2>
       
        
         <table class="table table-striped">
    <thead>
      <tr>
        <th>Documentos</th>
        
      </tr>
    </thead>
    <tbody>
      <tr>
          <c:forEach items="${documentos}" var="item">
       <tr> <td> ${item}</td></tr>
       </c:forEach>
      
    </tbody>
  </table>
      </div>
    </body>
</html>
